#!/usr/bin/env python3
from boilerplate import *
from sys import argv, exit

def lineToNgrams(n: int, line: list) -> Collector:
    if not line or n == 0: return Collector()
    if n == 1: return Collector(line)

    ngrams = Collector()
    i = 0; j = n
    while j <= len(line):
        ngrams.add(" ".join(line[i:j]))
        i += 1; j += 1
    return ngrams

def ngramsInFile(n: int, f) -> set:
    ngrams = Collector()
    for line in f:
        line = removePunctuation(line.strip().lower())
        line = line.split(" ") # split into words
        line = list(filter(bool, line)) # remove empty-string words
        ngrams.absorb(lineToNgrams(n, line))
    return ngrams

if __name__ == "__main__":
    try:
        MINIMUM = 0 if len(argv) < 3 else int(argv[2])
        N = int(argv[1])
    except IndexError:
        print("ngram.py <ngram length> [<minimum match>]")
        exit(2)

    with open("snark1.txt", 'r') as f:
        lines = f.readlines()
    printNgrams(N, ngramsInFile(N, lines), MINIMUM)
