#!/usr/bin/env python3
import string

class Collector(dict):
    '''Counts how many times an element has been added.'''

    def __init__(self, ngrams=[]):
        for ngram in ngrams:
            self.add(ngram)

    def absorb(self, other):
        for k in set(self.keys()).union(set(other.keys())):
            self[k] = self.get(k, 0) + other.get(k, 0)

    def add(self, ngram): self[ngram] = 1 + self.get(ngram, 0)
    def freqItems(self, minimum=0):
        items = self.items()
        if minimum:
            items = filter(lambda x: x[1] > minimum, items)
        return sorted(items, key=lambda x: x[1], reverse=True)

def removePunctuation(s: str) -> str:
    s = s.translate(str.maketrans("-", " "))
    return s.translate(str.maketrans("", "", string.punctuation))

def plural(n): return "s" if n > 1 else ""

def ngramName(n):
    nmap = {1: "uni", 2: "di", 3: "tri", 4: "quadri"}
    return nmap.get(n, str(n) + "-") + "gram"

def ntimesName(n):
    tmap = {1: "once", 2: "twice", 3: "thrice"}
    return tmap.get(n, str(n) + " times")

def printNgrams(n, ngrams, minimum=0):
    named = ngramName(n)
    num = len(ngrams)
    print("{} {}{}".format(num, named, plural(num)))
    p = [ "  {:15} {}".format(repr(ngram), ntimesName(count)) for ngram, count in ngrams.freqItems(minimum) ]
    if not p: return
    if minimum: print("the most frequent:")
    for pp in p:
        print(pp)
    print()
